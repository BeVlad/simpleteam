1 First step - start:
https://cloud.vk.com/docs/manage/tools-for-using-services/terraform/quick-start#937-tabpanel-1

2 Second step - data request:
vkcs_username
vkcs_password
vkcs_project_id

Infrastructure

![Screen](https://gitlab.com/BeVlad/simpleteam/-/blob/main/picture.png)

- Description of the infrastructure:

    - Load Balancer;
    - 2 WP application servers;
    - MySQL database failover cluster;
    - database backup;
    - fault-tolerant NFS-share;
    - monitoring server.


- Use the FIP of the monitoring server instance, port 3000  for access the Grafana    Web-interface.  Login: admin
- Upload the project administrator .PEM file (you need to generated yourself) to Settings/CI-CD Settings/Secure Files;
- Job artifact storage (1 h);
- To maintain the ability to delete infrastructure via terraform, you must download the artifact;
- The project uses Gitlab Shared runner


Variables:
| Name | Description |
| ------ | ------ |
|    GRAF_PASS (Masked)    |    password for Grafana administrator    |
|     TEL_BOT (Masked)   |    Telegram bot token    |
|     TEL_ID (Masked)   |    destination chat ID    |
|VKCS_PASSWORD (Masked)        |VK Cloud project password        |
|VKCS_PROJECT_ID        |VK Cloud project ID        |
|VKCS_USERNAME        |e-mail of the VK Cloud project administrator       |
|WORDPRESS_DB_PASSWORD (Masked)        |WP DB Password        |
|WORDPRESS_DB_USER        |WP DB username        |


