# Virtual Machines (VM) for monitoring

# CPU and RAM data for VM monitoring
data "vkcs_compute_flavor" "monitoring" {
  name = var.monitoring_flavor
}

# Image data for VM monitoring
data "vkcs_images_image" "monitoring" {
  name = var.monitoring_image
}

# Creating a computing resource for monitoring
resource "vkcs_compute_instance" "monitoring" {
  name                    = "monitoring"
  flavor_id               = data.vkcs_compute_flavor.monitoring.id
  key_pair                = var.key_pair_name
  security_groups         = [vkcs_networking_secgroup.secgroup.name]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.monitoring.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnet
  ]
}

# Displaying the VM IP address for monitoring
output "monitoring_ip" {
  value =     [vkcs_compute_instance.monitoring.network[0].fixed_ip_v4]
}
