# Description of the configuration for creating two Virtual Machines for Wordpress

# CPU and RAM data for Wordpress VM
data "vkcs_compute_flavor" "wordpress" {
  name = var.wordpress_flavor
}

# Image data for VM monitoring
data "vkcs_images_image" "wordpress" {
  name = var.wordpress_image
}

# Creating the first Wordpress VM
resource "vkcs_compute_instance" "wp_1" {
  name                    = "wordpress-1"
  flavor_id               = data.vkcs_compute_flavor.wordpress.id
  key_pair                = var.key_pair_name
  security_groups         = [vkcs_networking_secgroup.secgroup.name]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.wordpress.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnet
  ]
}

# Creating a second Wordpress VM
resource "vkcs_compute_instance" "wp_2" {
  name                    = "wordpress-2"
  flavor_id               = data.vkcs_compute_flavor.wordpress.id
  key_pair                = var.key_pair_name
  security_groups         = [vkcs_networking_secgroup.secgroup.name]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.wordpress.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_network.network,
    vkcs_networking_subnet.subnet
  ]
}

# Displaying IP addresses of VMs
output "instance_ips" {
  value = concat(
    [vkcs_compute_instance.wp_1.network[0].fixed_ip_v4],
    [vkcs_compute_instance.wp_2.network[0].fixed_ip_v4]
  )
}
