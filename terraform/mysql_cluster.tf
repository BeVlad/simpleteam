# We create a Galera MySQL database cluster with three nodes.
# Create a database, user and configure for automatic backup.

# Data about the CPU and RAM selected for the cluster VM.
data "vkcs_compute_flavor" "wp_db" {
  name = var.db_instance_flavor
}
# Creating a Galera MySQL database cluster.
# CPU and RAM transfer defined in var.db_instance_flavor variable.
# Storage size - 10 GB, storage type - Ceph SSD.
# The network defined in the vkcs_networking_network.network resource is used.
# The key pair defined in the var.key_pair_name variable is used as a key pair for accessing cluster nodes.
# Cluster size - 3 nodes
# Enabled automatic adjustment of disk size up to 1000 GB.
resource "vkcs_db_cluster" "wp_db_cluster" {
  name        = "wp-db-instance"
  availability_zone       = var.availability_zone_name
  datastore {
    type    = "galera_mysql"
    version = "8.0"
  }
  flavor_id   = data.vkcs_compute_flavor.wp_db.id
  keypair     = var.key_pair_name
  volume_size = 10
  volume_type = "ceph-ssd"
  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }
  cluster_size = 3

  network {
    uuid = vkcs_networking_network.network.id
  }

  depends_on = [
    vkcs_networking_router_interface.router_interface_lb
  ]

# You must enable the ability to export metrics in Prometheus format from the VM.
  capabilities {
    name = "node_exporter"
    settings = {
      "listen_port":"9100"
    }
  }
# You must enable the ability to export metrics in Prometheus format from the database.
  capabilities {
    name = "mysqld_exporter"
    settings = {
      "listen_port":"9110"
    }
  }
}

# Creating a database for WordPress.
resource "vkcs_db_database" "wp_db_database" {
  name        = "wordpressdb"
  dbms_id = vkcs_db_cluster.wp_db_cluster.id
  charset     = "utf8"
}

# Creating a user for the WordPress database.
resource "vkcs_db_user" "wp_db_user" {
  name        = var.WORDPRESS_DB_USER
  password    = var.WORDPRESS_DB_PASSWORD
  dbms_id = vkcs_db_cluster.wp_db_cluster.id
  databases   = [vkcs_db_database.wp_db_database.name]
}

# Create a backup plan for the database.
# The backup point - 12 hours, the maximum number of full backups - 3.
resource "vkcs_backup_plan" "backup_plan" {
  name          = "backup_wp_db_database"
  provider_name = "dbaas"
  incremental_backup = false
  schedule = {
    every_hours = 12
  }
  full_retention = {
    max_full_backup = 6
  }
  instance_ids = [vkcs_db_cluster.wp_db_cluster.id]
}

# Obtaining the load balancer IP address for the database cluster.
data "vkcs_lb_loadbalancer" "db_loadbalancer" {
  id = "${vkcs_db_cluster.wp_db_cluster.loadbalancer_id}"
}

data "vkcs_networking_port" "db_loadbalancer_port" {
  port_id = "${data.vkcs_lb_loadbalancer.db_loadbalancer.vip_port_id}"
}

# Displays the load balancer IP address for the database cluster.
output "cluster_lb_ip" {
  value = data.vkcs_networking_port.db_loadbalancer_port.all_fixed_ips
}
