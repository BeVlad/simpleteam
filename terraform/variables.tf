# Variables for setting up and connecting to a VK Cloud account 

variable "VKCS_USERNAME" {
 type        = string
 description = "VK Cloud account email"
 default     = "test@mail.com"
}

variable "VKCS_PASSWORD" {
 type        = string
 description = "VK Cloud account password"
 default     = "******"
}

variable "VKCS_PROJECT_ID" {
 type        = string
 description = "Project ID in VK Cloud"
 default     = "*******"
}

variable "GRAF_PASS" {
 type        = string
 description = "Grafana password"
 default     = "*******"
}

variable "TEL_BOT" {
 type        = string
 description = "Telegram bot"
 default     = "*******"
}

variable "TEL_ID" {
 type        = string
 description = "Telegram bot ID"
 default     = "*******"
}

variable "WORDPRESS_DB_USER" {
 type        = string
 description = "Wordpress database username"
 default     = "wordpress"
}

variable "WORDPRESS_DB_PASSWORD" {
 type        = string
 description = "Wordpress database password"
 default     = "Password1234567892!"
}

variable "wordpress_flavor" {
 type        = string
 description = "CPU и RAM для ВМ Wordpress"
 default     = "STD2-2-4"
}

variable "wordpress_image" {
 type        = string
 description = "Image for Wordpress VM"
 default     = "Ubuntu-22.04-202208"
}

variable "monitoring_flavor" {
 type        = string
 description = "CPU and RAM for VM monitoring"
 default     = "STD2-2-4"
}

variable "monitoring_image" {
 type        = string
 description = "Image for VM monitoring"
 default     = "Ubuntu-22.04-202208"
}

variable "key_pair_name" {
 type        = string
 description = "Key pair name for all VMs"
 default     = "simplexteam"
}

variable "availability_zone_name" {
 type        = string
 description = "Availability zone name"
 default     = "MS1"
}

variable "db_instance_flavor" {
 type        = string
 description = "CPU and RAM for database cluster VMs"
 default     = "STD2-2-8"
}
