# Network structure

# Getting an external network
data "vkcs_networking_network" "extnet" {
   name = "ext-net"
}

# Creation of network-int-net
resource "vkcs_networking_network" "network" {
   name = "manage-int-net"
}

# Creating a subnet in the manage-int-net network
resource "vkcs_networking_subnet" "subnet" {
   name       = "subnet"
   network_id = vkcs_networking_network.network.id
   cidr       = "10.10.10.0/24"
}

# Creating a router
resource "vkcs_networking_router" "router" {
  name = "router"
  external_network_id = data.vkcs_networking_network.extnet.id
}

# Connecting the subnet subnet to the router router
resource "vkcs_networking_router_interface" "router_interface_lb" {
  router_id = vkcs_networking_router.router.id
  subnet_id = vkcs_networking_subnet.subnet.id
}

# Creating a db-port on the manage-int-net network
resource "vkcs_networking_port" "db_port" {
  name           = "db-port"
  network_id     = vkcs_networking_network.network.id
  admin_state_up = true
}

# Create a public IP for the load balancer
resource "vkcs_networking_floatingip" "lb_fip" {
  pool    = data.vkcs_networking_network.extnet.name  
  port_id = vkcs_lb_loadbalancer.loadbalancer.vip_port_id
}

# Creating a public IP for monitoring
resource "vkcs_networking_floatingip" "monitoring_fip" {
  pool    = data.vkcs_networking_network.extnet.name
}

# Linking public IP monitoring to a monitoring instance
resource "vkcs_compute_floatingip_associate" "monitoring_fip" {
  floating_ip = vkcs_networking_floatingip.monitoring_fip.address
  instance_id = vkcs_compute_instance.monitoring.id
}

# Creating a public IP for wp1
resource "vkcs_networking_floatingip" "wp1_fip" {
  pool    = data.vkcs_networking_network.extnet.name
}

# Linking wp1 public IP to wp1 instance
resource "vkcs_compute_floatingip_associate" "wp1_fip" {
  floating_ip = vkcs_networking_floatingip.wp1_fip.address
  instance_id = vkcs_compute_instance.wp_1.id
}

# Creating a public IP for wp2
resource "vkcs_networking_floatingip" "wp2_fip" {
  pool    = data.vkcs_networking_network.extnet.name
}

# Linking wp2 public IP to wp2 instance
resource "vkcs_compute_floatingip_associate" "wp2_fip" {
  floating_ip = vkcs_networking_floatingip.wp2_fip.address
  instance_id = vkcs_compute_instance.wp_2.id
}

# Public IP monitoring output
output "monitoring_instance_fip" {
  value = vkcs_networking_floatingip.monitoring_fip.address
}

# Output of public IP load balancer
output "lb_instance_fip" {
  value = vkcs_networking_floatingip.lb_fip.address
}
