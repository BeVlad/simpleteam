# Load balancer (LB)

# Creating LB
resource "vkcs_lb_loadbalancer" "loadbalancer" {
  name = "loadbalancer"
  vip_subnet_id = vkcs_networking_subnet.subnet.id
}

# Creating a Listener
resource "vkcs_lb_listener" "listener" {
  name = "listener"
  protocol = "HTTP"
  protocol_port = 80
  loadbalancer_id = vkcs_lb_loadbalancer.loadbalancer.id
}

# Creating a pool and balancing type
resource "vkcs_lb_pool" "pool" {
  name = "pool"
  protocol = "HTTP"
  lb_method = "ROUND_ROBIN"
  listener_id = vkcs_lb_listener.listener.id
}

# Create Pool Member №1
resource "vkcs_lb_member" "member_1" {
  address = vkcs_compute_instance.wp_1.network[0].fixed_ip_v4
  protocol_port = 80
  pool_id = vkcs_lb_pool.pool.id
  subnet_id = vkcs_networking_subnet.subnet.id
  weight = 5
}

# Create Pool Member №2
resource "vkcs_lb_member" "member_2" {
  address = vkcs_compute_instance.wp_2.network[0].fixed_ip_v4
  protocol_port = 80
  pool_id = vkcs_lb_pool.pool.id
  subnet_id = vkcs_networking_subnet.subnet.id
  weight = 1
}
