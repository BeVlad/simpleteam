# Creating NFS shares for WordPress

# Sharеnetwork и sharе
resource "vkcs_sharedfilesystem_sharenetwork" "sharenetwork" {
  name                = "nfs_sharenetwork"
  neutron_net_id      = "${vkcs_networking_network.network.id}"
  neutron_subnet_id   = "${vkcs_networking_subnet.subnet.id}"
}

resource "vkcs_sharedfilesystem_share" "share" {
  name             = "nfs_share"
  share_proto      = "NFS"
  share_type       = "default_share_type"
  size             = 10
  share_network_id = "${vkcs_sharedfilesystem_sharenetwork.sharenetwork.id}"
}

# Providing share access for two Wordpress VMs
resource "vkcs_sharedfilesystem_share_access" "wp_access_1" {
  share_id     = "${vkcs_sharedfilesystem_share.share.id}"
  access_type  = "ip"
  access_to    = vkcs_compute_instance.wp_1.network[0].fixed_ip_v4
  access_level = "rw"
}

resource "vkcs_sharedfilesystem_share_access" "wp_access_2" {
  share_id     = "${vkcs_sharedfilesystem_share.share.id}"
  access_type  = "ip"
  access_to    = vkcs_compute_instance.wp_2.network[0].fixed_ip_v4
  access_level = "rw"
}

# Defining share mount options
locals {
  mount_type = lower(vkcs_sharedfilesystem_share.share.share_proto)
  mount_device = vkcs_sharedfilesystem_share.share.export_location_path
  mount_dir = "/${vkcs_sharedfilesystem_share.share.name}"
}

# Share mount command output
output "mount" {
  value = "mount -t ${local.mount_type} ${local.mount_device} ${local.mount_dir}"
  description = "Mount to vkcs_sharedfilestystem_share"
}
