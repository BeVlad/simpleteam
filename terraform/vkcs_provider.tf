terraform {
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
            version = "~> 0.6.0" 
        }
    }
}

provider "vkcs" {
    # Here indicate your username, password and project_id to access the Mail.Ru cloud 
    username = var.VKCS_USERNAME 
    password = var.VKCS_PASSWORD
    project_id = var.VKCS_PROJECT_ID
    # Region name
    region = "RegionOne"
    # Login URL
    auth_url = "https://infra.mail.ru:35357/v3/" 
}
